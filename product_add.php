<?php
include 'classes.inc.php';
$errors = [];
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="dfd.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("select").change(function(){
                $(this).find("option:selected").each(function(){
                    var optionValue = $(this).attr("value");
                    if (optionValue) {
                        $("#DVD_Disc").not("." + optionValue).hide();
                        $("#Book").not("." + optionValue).hide();
                        $("#Furniture").not("." + optionValue).hide();
                        $("#" + optionValue).show();
                    } else {
                        $(".box").hide();
                    }
                });
            }).change();
        });
    </script>
</head>

<body>
<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post" name="ProductAddForm" id="ProductAddForm">
    <?php
    if (isset($_POST['save'])) {
        $missing = new MissingComponent($_POST);
        try {
            $missing->validateForm();
        } catch (Exception $e) {
            $errors = $missing->getErrors();
        }

        try {
            if (empty($errors)) {
                $product = new Products($_POST);
                try {
                    if ($product->save()) {
                        echo "<script>alert('Product inserted')</script>";
                    }
                } catch (Exception $e){
                    echo $e->getMessage();
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    ?>
    <nav class="navbar navbar-expand-lg py-3 navbar-dark bg-dark shadow-sm">
        <div class="container">
            <a href="product_list.php" class="navbar-brand">
                <img src="551167_7840631_1559024_911ff84c_image.png" width="150" alt="Scaniweb">
            </a>
            <h1>Product add</h1>
            <input type="submit" name="save" id="save" class="btn btn-secondary btn-lg" value="Save"/>
        </div>
    </nav>
    <div class="container">
        <div class="form-group">
        <label for="sku" class="col-sm-2 control-label mt-5">SKU:</label>
        <div class="form-group col-lg-2">
            <input type="text" class="form-control" value="<?=htmlspecialchars($_POST['sku'] ?? '')   ?>" placeholder="Enter SKU" name="sku" id="sku"/>
            <div class="error">
                <span class="badge badge-dark"><?=$errors['sku']?? '' ?></span>
            </div>
        </div>
        <label for="name" class="col-sm-2 control-label">Name:</label>
        <div class="form-group col-lg-2">
        <input type="text" class="form-control" value="<?=htmlspecialchars($_POST['name'] ?? '')  ?>" placeholder="Enter name" name="name" id="name"/>
            <div class="error">
                <span class="badge badge-dark"><?=$errors['name']?? '' ?></span>
            </div>
        </div>
        <label for="price" class="col-sm-2 control-label">Price:</label>
        <div class="form-group col-lg-2">
            <input type="text" class="form-control" value="<?=htmlspecialchars($_POST['price'] ?? '')  ?>" placeholder="Enter price" name="price" id="price"/>
            <div class="error">
                <span class="badge badge-dark"><?=$errors['price']?? '' ?></span>
            </div>
        </div>
        <label for="type_switcher" class="col-sm-2 control-label">Type Switcher:

        </label>
        <select class="form-control col-lg-2 ml-3" name="type_switcher" id="type_switcher">
            <option value="hide"  disabled="disabled">Select product type</option>
            <option value="DVD_Disc" >DVD-disc</option>
            <option value="Book" <?=(isset($_POST['type_switcher']) && $_POST['type_switcher']=='Book')?"selected":""?>>Book</option>
            <option value="Furniture" <?=(isset($_POST['type_switcher']) && $_POST['type_switcher']=='Furniture')?"selected":""?>>Furniture</option>
        </select>
        <p/>
        <div id="hide" name="hide">
        </div>
        <div class="DVD_Disc box" id="DVD_Disc">
            <label for="size" class="col-sm-2 control-label">Size:</label>
            <div class="form-group col-lg-2">
                <input type="text" class="form-control" value="<?=htmlspecialchars($_POST['size'] ?? '')  ?>" placeholder="Enter size" name="size" id="size">
                <div class="error">
                    <span class="badge badge-dark"><?=$errors['size']?? '' ?></span>
                </div>
            </div>
        </div>
        <div class="Book box" id="Book">
            <label for="Weight" class="col-sm-2 control-label">Weight:</label>
            <div class="form-group col-lg-2">
                <input type="text" class="form-control" value="<?=htmlspecialchars($_POST['weight'] ?? '')  ?>" placeholder="Enter weight" name="weight" id="weight">
                <div class="error">
                    <span class="badge badge-dark"><?=$errors['weight']?? '' ?></span>
                </div>
            </div>
        </div>
        <div class="Furniture box" id="Furniture">
            <label for="height" class="col-sm-2 control-label">Height:</label>
            <div class="form-group col-lg-2">
                <input type="text" class="form-control" value="<?=htmlspecialchars($_POST['height'] ?? '')  ?>" placeholder="Enter height" name="height" id="height">
                <div class="error">
                    <span class="badge badge-dark"><?=$errors['height']?? '' ?></span>
                </div>
            </div>
            <label for="width" class="col-sm-2 control-label">Width:</label>
            <div class="form-group col-lg-2">
                <input type="text" class="form-control" value="<?=htmlspecialchars($_POST['width'] ?? '')  ?>" placeholder="Enter width" name="width" id="width">
                <div class="error">
                    <span class="badge badge-dark"><?=$errors['width']?? '' ?></span>
                </div>
            </div>
            <label for="length" class="col-sm-2 control-label">Length:</label>
            <div class="form-group col-lg-2">
                <input type="text" class="form-control" value="<?=htmlspecialchars($_POST['length'] ?? '')  ?>" placeholder="Enter length" name="length" id="length">
                <div class="error">
                    <span class="badge badge-dark"><?=$errors['length']?? '' ?></span>
                </div>
            </div>
        </div>
        </div>
    </div>
</form>
</body>
</html>