<?php
include 'DB_Connect.php';

class Products {
    private $sku;
    private $name;
    private $price;
    private $type;
    private $size;
    private $weight;
    private $height;
    private $width;
    private $length;

    public function __construct($data) {
        $this->setSku($data['sku']);
        $this->setName($data['name']);
        $this->setPrice($data['price']);
        $this->setType($data['type_switcher']);
        $this->setSize($data['size']);
        $this->setWeight($data['weight']);
        $this->setHeight($data['height']);
        $this->setWidth($data['width']);
        $this->setLength($data['length']);
    }

    public function getSku()
    {
        return $this->sku;
    }

    public function setSku($sku): void
    {
        $this->sku = $sku;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price): void
    {
        if (is_numeric($price)) {
            $this->price = $price;
        }
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type): void
    {
        $this->type = $type;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size): void
    {
        if (is_numeric($size)) {
            $this->size = $size;
        }
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function setWeight($weight): void
    {
        if (is_numeric($weight)) {
            $this->weight = $weight;
        }
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setHeight($height): void
    {
        if (is_numeric($height)) {
            $this->height = $height;
        }
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function setWidth($width): void
    {
        if (is_numeric($width)) {
            $this->width = $width;
        }
    }

    public function getLength()
    {
        return $this->length;
    }

    public function setLength($length): void
    {
        if (is_numeric($length)) {
            $this->length = $length;
        }
    }

    public function save() {
        $data = new DBProducts();
        return $data->insert($this);
    }

    public function toArray() {
        return get_object_vars($this);
    }
}

class MissingComponent {
    private $data;
    private $errors = [];
    private static $fields = ['sku','name','price','size','weight','height','width','length'];
    private static $types = [
        'DVD_Disc' => ['validateSize'],
        'Book' => ['validateWeight'],
        'Furniture' => ['validateHeight','validateWidth','validateLength']
        ];

    public function getErrors() {
        return $this->errors;
    }

    public function __construct($product) {
        $this->data = $product;
    }

    public function validateForm() {
        foreach(self::$fields as $field) {
            if(!array_key_exists($field, $this->data)) {
                trigger_error("$field is not present in data");
                return;
            }
        }

        $this->validateSKU();
        $this->validateName();
        $this->validatePrice();

        foreach (self::$types[$this->data['type_switcher']] as $type) {
            $this->{$type}();
        }

        if (!empty($this->errors)) {
            throw new \Exception('Error: validation is not correct');
        }
    }

    private function validateSKU() {
        $val = trim($this->data['sku']);
        if (empty($val)) {
            $this->addError('sku', 'Please add SKU');
        } else {
            if (!preg_match('/^[a-zA-Z0-9]{6,12}$/', $val)) {
                $this->addError('sku','SKU must be at least 6-12 chars & alphanumeric');
            }
        }
    }

    private function validateName() {
        $val = trim($this->data['name']);
        if (empty($val)) {
            $this->addError('name', 'Please add name');
        } else {
            if (!preg_match('/^[a-zA-Z0-9]{6,12}$/', $val)) {
                $this->addError('name','Name must be at least 6-12 chars & alphanumeric');
            }
        }
    }

    private function validatePrice() {
        $val = trim($this->data['price']);
        if (empty($val)) {
            $this->addError('price', 'Please add price');
        } else {
            if (!preg_match('/^[0-9]+$/', $val)) {
                $this->addError('price','Price must be numeric');
            }
        }
    }

    private function validateSize() {
        $val = trim($this->data['size']);
        if (empty($val)) {
            $this->addError('size', 'Please add size');
        } else {
            if (!preg_match('/^[0-9]+$/', $val)) {
                $this->addError('size','Size must be numeric');
            }
        }
    }

    private function validateWeight() {
        $val = trim($this->data['weight']);
        if (empty($val)) {
            $this->addError('weight', 'Please add weight');
        } else {
            if (!preg_match('/^[0-9]+$/', $val)) {
                $this->addError('weight','Weight must be numeric');
            }
        }
    }

    private function validateHeight() {
        $val = trim($this->data['height']);
        if (empty($val)) {
            $this->addError('height', 'Please add height');
        } else {
            if (!preg_match('/^[0-9]+$/', $val)) {
                $this->addError('height','Height must be numeric');
            }
        }
    }

    private function validateWidth() {
        $val = trim($this->data['width']);
        if (empty($val)) {
            $this->addError('width', 'Please add width');
        } else {
            if (!preg_match('/^[0-9]+$/', $val)) {
                $this->addError('width','Width must be numeric');
            }
        }
    }

    private function validateLength() {
        $val = trim($this->data['length']);
        if (empty($val)) {
            $this->addError('length', 'Please add length');
        } else {
            if (!preg_match('/^[0-9]+$/', $val)) {
                $this->addError('length','Length must be numeric');
            }
        }
    }

    private function addError($key, $val) {
        $this->errors[$key] = $val;
    }
}

class DBProducts extends DB_Connect {

    public function insert($product) {
        $cols = array_keys($product->toArray());
        $colsCsv = implode(', ',$cols);
        $valuesCSV = implode(',:',$cols);
        try {
        $sql_Query = "INSERT INTO products ($colsCsv) VALUES (:$valuesCSV)";
        $stmt = $this->connect()->prepare($sql_Query);
        $stmt->execute($product->toArray());
        return true;
        } catch (PDOException $e) {
            echo "Error: ". $e->getMessage();
            return false;
        }
    }

    public function select() {
        try {
            $sql_Query = "SELECT * FROM products ORDER BY product_id";
            $stmt = $this->connect()->prepare($sql_Query);
            $stmt->execute();
            $stmt_fetch = $stmt->fetchAll();
            return $stmt_fetch;
        } catch (PDOException $e) {
            echo "Error: ". $e->getMessage();
        }
    }

    public function delete() {
        try {
            if (isset($_POST['id'])) {
                foreach ($_POST['id'] as $id) {
                    $sql_Query= "DELETE from products WHERE product_id=" . $id;
                    $stmt = $this->connect()->prepare($sql_Query);
                    $stmt->execute();
                }
            }
        } catch (PDOException $e) {
            echo "Error: ".$e->getMessage();
        } return true;
    }
}
?>