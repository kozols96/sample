drop database if exists products;

create database products;

use products;

create table products (
	product_id int not null auto_increment,
    sku varchar(100),
    name varchar(100),
    price float,
    type varchar(100),
    size int,
    weight float,
    height float,
    width float,
    length float,
    primary key (product_id)
);

