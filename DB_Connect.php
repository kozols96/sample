<?php

class DB_Connect {

    private $servername;
    private $username;
    private $password;
    private $dbname;
    private $charset;
    private $connection;

    public function connect() {
        if ($this->connection == null) {
            $this->servername = "localhost";
            $this->username = "root";
            $this->password = "140694Go@#";
            $this->dbname = "products";
            $this->charset = "utf8mb4";

            try {
                $dsn = "mysql:host=" . $this->servername . ";dbname=" . $this->dbname . ";charset=" . $this->charset;
                $pdo = new PDO($dsn, $this->username, $this->password);
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $this->connection = $pdo;
            } catch (PDOException $e) {
                echo "Connection failed: " . $e->getMessage();
            }
        } return $this->connection;
    }
}