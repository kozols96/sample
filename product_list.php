<?php

include 'classes.inc.php';
$data = new DBProducts();
if (isset($_POST['apply'])) {
    if ($_POST['list_decision'] == "Mass_Delete") {
        $data->delete();
    }
}
?>

<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="dfd.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>

<body>
<form action="<?= $_SERVER['PHP_SELF']; ?>" method="post" name="ProductListForm" id="ProductListForm">
<nav class="navbar navbar-expand-lg py-3 navbar-dark bg-dark shadow-sm">
    <div class="container">
        <a href="product_list.php" class="navbar-brand">
            <img src="551167_7840631_1559024_911ff84c_image.png" width="150" alt="Scaniweb">
        </a>
        <h1>Product list</h1>
        <a href="product_add.php" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">Add</a>
    </div>
</nav>
<div class="container">
    <input type="submit" name="apply" id="apply" class="btn btn-secondary mt-5 btn-md float-right" value="Apply"/>
    <select class="form-control col-lg-2 mt-5 float-right" name="list_decision" id="list_decision">
        <option value="empty"></option>
        <option value="Mass_Delete">Mass Delete Action</option>
    </select>
</div>
<p>
<div class="container">
    <h2 class="mt-5">List of Products</h2>
    <div class="row mt-5">
        <?php
        $post_data = $data->select();
        foreach($post_data as $post) {
            ?>
            <div class="col-md-3 mt-3">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="id[]" value="<?=$post['product_id']?>">
                <div class="card text-center">
                    <div><?php echo "Product ID: ".$post["product_id"]?></div>
                    <div><?php echo "SKU: ".$post["sku"]?></div>
                    <div><?php echo "Name: ".$post["name"]?></div>
                    <div><?php echo "Price: ".$post["price"]." $"?></div>
                    <?php if ($post["type"] == 'DVD_Disc') {?>
                    <div><?php echo "Size: ".$post["size"]." MB"?></div>
                    <?php } ?>
                    <?php if ($post["type"] == 'Book') {?>
                    <div><?php echo "Weight: ".$post["weight"]." KG"?></div>
                    <?php }?>
                    <?php if ($post["type"] == 'Furniture') {?>
                    <div><?php echo "Dimensions: ".$post["height"]."x".$post["width"]."x".$post["length"]?></div>
                    <?php }?>
                </div>
                </div>
            </div>
        <?php }?>
    </div>
</div>
</form>
</body>
</html>